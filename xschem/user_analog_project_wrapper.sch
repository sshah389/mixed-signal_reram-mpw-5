v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N -580 -380 -550 -380 { lab=io_analog[4]}
N -510 -540 -510 -450 { lab=io_analog[0]}
N -510 -450 -510 -440 { lab=io_analog[0]}
N -430 -510 -430 -440 { lab=io_analog[1]}
N -430 -540 -430 -510 { lab=io_analog[1]}
N -390 -340 -350 -340 { lab=vssa1}
N -580 -340 -550 -340 { lab=io_analog[5]}
N -510 -280 -510 -260 {
lab=io_analog[2]}
N -430 -280 -430 -260 {
lab=io_analog[3]}
N 280 -460 280 -400 {
lab=vssa1}
N 280 -460 300 -460 {
lab=vssa1}
N 300 -460 300 -400 {
lab=vssa1}
N 290 -560 290 -460 {
lab=vssa1}
N 280 -280 280 -220 {
lab=vccd1}
N 280 -220 300 -220 {
lab=vccd1}
N 300 -280 300 -220 {
lab=vccd1}
N 290 -220 290 -160 {
lab=vccd1}
N 820 -460 820 -390 {
lab=vssa1}
N 820 -460 840 -460 {
lab=vssa1}
N 840 -460 840 -390 {
lab=vssa1}
N 830 -540 830 -460 {
lab=vssa1}
N 830 -560 830 -540 {
lab=vssa1}
N 820 -270 820 -220 {
lab=vccd1}
N 820 -220 840 -220 {
lab=vccd1}
N 840 -270 840 -220 {
lab=vccd1}
N 830 -220 830 -160 {
lab=vccd1}
N 700 -330 760 -330 {
lab=io_in[13]}
N 920 -330 960 -330 {
lab=io_out[13]}
N 960 -330 1000 -330 {
lab=io_out[13]}
N 700 -300 760 -300 {
lab=io_analog[6]}
N 700 -360 760 -360 {
lab=io_analog[7]}
N 370 -340 440 -340 {
lab=gpio_analog[6]}
N 120 -360 210 -360 {
lab=gpio_analog[7]}
N 120 -320 210 -320 {
lab=gpio_analog[8]}
N -430 -1030 -380 -1030 { lab=vssa2}
N -430 -940 -380 -940 { lab=io_analog[9]}
N -610 -990 -560 -990 { lab=io_analog[8]}
N -480 -1080 -480 -1040 { lab=io_analog[9]}
C {devices/lab_pin.sym} -430 -540 2 0 {name=l1 lab=io_analog[1]}
C {devices/lab_pin.sym} -510 -540 0 0 {name=l2 lab=io_analog[0]}
C {devices/lab_pin.sym} -580 -380 0 0 {name=l3 lab=io_analog[4]}
C {devices/lab_pin.sym} -580 -340 0 0 {name=l4 lab=io_analog[5]}
C {devices/lab_pin.sym} -510 -260 0 0 {name=l5 lab=io_analog[2]}
C {devices/lab_pin.sym} -430 -260 2 0 {name=l6 lab=io_analog[3]}
C {devices/lab_pin.sym} -355 -340 2 0 {name=l9 lab=vssa1}
C {1T1R_2x2.sym} -470 -360 0 0 {name=x1}
C {devices/iopin.sym} -1080 -1140 0 0 {name=p7 lab=vdda1}
C {devices/iopin.sym} -1080 -1110 0 0 {name=p8 lab=vdda2}
C {devices/iopin.sym} -1080 -1080 0 0 {name=p10 lab=vssa1}
C {devices/iopin.sym} -1080 -1050 0 0 {name=p11 lab=vssa2}
C {devices/iopin.sym} -1080 -1020 0 0 {name=p12 lab=vccd1}
C {devices/iopin.sym} -1080 -990 0 0 {name=p13 lab=vccd2}
C {devices/iopin.sym} -1080 -960 0 0 {name=p14 lab=vssd1}
C {devices/iopin.sym} -1080 -930 0 0 {name=p15 lab=vssd2}
C {devices/ipin.sym} -1030 -860 0 0 {name=p16 lab=wb_clk_i}
C {devices/ipin.sym} -1030 -830 0 0 {name=p17 lab=wb_rst_i}
C {devices/ipin.sym} -1030 -800 0 0 {name=p18 lab=wbs_stb_i}
C {devices/ipin.sym} -1030 -770 0 0 {name=p19 lab=wbs_cyc_i}
C {devices/ipin.sym} -1030 -740 0 0 {name=p20 lab=wbs_we_i}
C {devices/ipin.sym} -1030 -710 0 0 {name=p21 lab=wbs_sel_i[3:0]}
C {devices/ipin.sym} -1030 -680 0 0 {name=p22 lab=wbs_dat_i[31:0]}
C {devices/ipin.sym} -1030 -650 0 0 {name=p23 lab=wbs_adr_i[31:0]}
C {devices/opin.sym} -1040 -590 0 0 {name=p24 lab=wbs_ack_o}
C {devices/opin.sym} -1040 -560 0 0 {name=p25 lab=wbs_dat_o[31:0]}
C {devices/ipin.sym} -1030 -520 0 0 {name=p26 lab=la_data_in[127:0]}
C {devices/opin.sym} -1040 -490 0 0 {name=p27 lab=la_data_out[127:0]}
C {devices/ipin.sym} -1030 -410 0 0 {name=p28 lab=io_in[26:0]}
C {devices/ipin.sym} -1030 -380 0 0 {name=p29 lab=io_in_3v3[26:0]}
C {devices/ipin.sym} -1040 -100 0 0 {name=p30 lab=user_clock2}
C {devices/opin.sym} -1040 -350 0 0 {name=p31 lab=io_out[26:0]}
C {devices/opin.sym} -1040 -320 0 0 {name=p32 lab=io_oeb[26:0]}
C {devices/iopin.sym} -1070 -260 0 0 {name=p33 lab=gpio_analog[17:0]}
C {devices/iopin.sym} -1070 -230 0 0 {name=p34 lab=gpio_noesd[17:0]}
C {devices/iopin.sym} -1070 -200 0 0 {name=p35 lab=io_analog[10:0]}
C {devices/iopin.sym} -1070 -170 0 0 {name=p36 lab=io_clamp_high[2:0]}
C {devices/iopin.sym} -1070 -140 0 0 {name=p37 lab=io_clamp_low[2:0]}
C {devices/opin.sym} -1050 -70 0 0 {name=p38 lab=user_irq[2:0]}
C {devices/ipin.sym} -1030 -460 0 0 {name=p39 lab=la_oenb[127:0]}
C {sky130_sc_ams__ota_1.sym} 290 -340 0 0 {name=x2}
C {sky130_sc_ams__comparator_1.sym} 830 -330 0 0 {name=x3}
C {devices/lab_pin.sym} 290 -560 0 0 {name=l7 sig_type=std_logic lab=vssa1}
C {devices/lab_pin.sym} 290 -160 0 0 {name=l8 sig_type=std_logic lab=vccd1
}
C {devices/lab_pin.sym} 830 -160 0 0 {name=l10 sig_type=std_logic lab=vccd1}
C {devices/lab_pin.sym} 830 -560 0 0 {name=l11 sig_type=std_logic lab=vssa1
}
C {devices/lab_pin.sym} 700 -330 0 0 {name=l12 sig_type=std_logic lab=io_in[13]
}
C {devices/lab_pin.sym} 1000 -330 2 0 {name=l13 sig_type=std_logic lab=io_out[13]
}
C {devices/lab_pin.sym} 700 -360 0 0 {name=l14 sig_type=std_logic lab=io_analog[7]
}
C {devices/lab_pin.sym} 700 -300 0 0 {name=l15 sig_type=std_logic lab=io_analog[6]
}
C {devices/lab_pin.sym} 440 -340 2 0 {name=l16 sig_type=std_logic lab=gpio_analog[6]}
C {devices/lab_pin.sym} 120 -360 0 0 {name=l17 sig_type=std_logic lab=gpio_analog[7]
}
C {devices/lab_pin.sym} 120 -320 0 0 {name=l18 sig_type=std_logic lab=gpio_analog[8]
}
C {FG_pfet.sym} -410 -970 0 0 {name=x4}
C {devices/lab_pin.sym} -380 -1030 2 0 {name=l19 lab=vssa2}
C {devices/lab_pin.sym} -380 -940 2 0 {name=l20 lab=io_analog[9]}
C {devices/lab_pin.sym} -610 -990 0 0 {name=l21 lab=io_analog[8]}
C {devices/lab_pin.sym} -480 -1080 1 0 {name=l22 lab=io_analog[10]}
